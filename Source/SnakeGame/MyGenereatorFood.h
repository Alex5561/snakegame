// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MyGenereatorFood.generated.h"

class AFood;

UCLASS()
class SNAKEGAME_API AMyGenereatorFood : public AActor
{
	GENERATED_BODY()

		UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AFood>FoodElementClass;

	
	
public:	
	// Sets default values for this actor's properties
	AMyGenereatorFood();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	float minX = -1460.f;
	float maxX = 1420.f;
	float minY = -3900.f;
	float maxY = 3920.f;
	float SpawnZ = -40.f;
	float StepDelay = 1.f;
	float BTime = 0;

	void RandomFood();
};
