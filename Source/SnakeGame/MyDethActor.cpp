// Fill out your copyright notice in the Description page of Project Settings.


#include "MyDethActor.h"
#include "SnakeGame.h"
#include "SnakeBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"




// Sets default values
AMyDethActor::AMyDethActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);


}

// Called when the game starts or when spawned
void AMyDethActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMyDethActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	CollideWall();

}

void AMyDethActor::CollideWall()
{
	TArray<AActor*>CollectedActors;

	GetOverlappingActors(CollectedActors);

	for (int32 i = 0; i < CollectedActors.Num(); ++i)
	{
		CollectedActors[i]->Destroy(true, true);
	}

}

